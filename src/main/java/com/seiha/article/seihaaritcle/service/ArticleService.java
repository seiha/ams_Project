package com.seiha.article.seihaaritcle.service;

import com.seiha.article.seihaaritcle.model.Article;
import com.seiha.article.seihaaritcle.model.ArticleFilter;

import java.util.List;

public interface ArticleService {
    void insert(Article article);
    void update(Article article);
    void delete(int id);
    Article findOne(int id);
    List<Article> findAll();

    List<Article> findAllFilter(ArticleFilter filter);
}
